#include <iostream>
#include <string>
#include <cstring>

int main(int argc, char *argv[]) {
	std::string mensagem;

	if (argc == 2 && strcmp(argv[1], "add") == 0) {
		std::cout << "Informe seu mensagem: ";
		std:getline(std::cin, mensagem);
	}
	else if (argc == 3 && strcmp(argv[1], "add") == 0) {
		mensagem = argv[2];
	}
	else {
		std::cout << "Erro!" << std::endl;
		std::cout << "Use: " << argv[0] << " add" << std::endl;
		std::cout << "Ou" << std::endl;
		std::cout << "Use: " << argv[0] << " add <mensagem>" << std::endl;
		return -1;
	}

	std::cout << "Mensagem Adicionada: " << mensagem << std::endl;

	return 0;
}