#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char *argv[]) {
	std::string mensagem;

	if (argc == 2 && std::string(argv[1]) == "list") {
		std::ifstream arquivo_entrada{"diario.txt"};

		while (!arquivo_entrada.eof()) {
			std::getline(arquivo_entrada, mensagem);
			if (mensagem != "")
				std::cout << mensagem << std::endl;
		}
		arquivo_entrada.close();

		return 0;
	}
	else if (argc == 2 && std::string(argv[1]) == "add") {
		std::cout << "Informe seu mensagem: ";
		std:getline(std::cin, mensagem);
	}
	else if (argc == 3 && std::string(argv[1]) == "add") {
		mensagem = argv[2];
	}
	else {
		std::cout << "Erro!" << std::endl;
		std::cout << "Use: " << argv[0] << " add" << std::endl;
		std::cout << "Ou" << std::endl;
		std::cout << "Use: " << argv[0] << " add <mensagem>" << std::endl;
		return -1;
	}

	std::ofstream arquivo_saida{"diario.txt", std::ios::app};
	arquivo_saida << mensagem << std::endl;
	std::cout << "Mensagem Adicionada: " << mensagem << std::endl;
	arquivo_saida.close();

	return 0;
}